import os, sys
from configparser import ConfigParser
import smtplib
from typing import List

from misc import email_conf, emails


# def prompt(prompt):
#     return input(prompt).strip()

def send_email(subject: str, to: List[str], cc: List[str], bcc: List[str], body: str):
    """Send an email"""
    base_path = os.path.dirname(os.path.abspath(__file__))
    config_path = os.path.join(base_path, 'email.ini')

    if os.path.exists(config_path):
        cfg = ConfigParser()
        cfg.read(config_path)
    else:
        print('Config not found! Exiting!')
        sys.exit(1)

    host = cfg.get('smtp', 'server')
    from_addr = cfg.get('smtp', 'from_addr')
    body = '\r\n'.join((
        f'From: {from_addr}',
        f'To: {", ".join(to)}',
        f'Cc: {", ".join(cc)}',
        f'Subject: {subject}\n',
        body
    ))
    with smtplib.SMTP('smtp.gmail.com: 587') as server:
        server.starttls()
        server.login(email_conf['login'], email_conf['password'])
        server.set_debuglevel(1)
        emails = to + cc + bcc
        server.sendmail(from_addr, emails, body)

if __name__ == '__main__':
    subject = 'HELLO VDIM'
    to = emails['to']
    cc = emails['cc']
    bcc = emails['bcc']
    body = 'hello, from simple_smtp module' 
    send_email(subject, to, cc, bcc, body)
    