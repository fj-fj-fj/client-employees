# import psycopg2
from misc import dbname, user, host, password


# connect = psycopg2.connect(
#     dbname=dbname, user=user, host=host, password=password
# )
# try:
#     with connect:
#         with connect.cursor()
#         with open('northwind.sql', encoding='utf-8') as sql_script:
#             query = ''.join(sql_script.readlines())
#             cur.execute(query)
#     with connect:
#         cur.execute("SELECT * FROM products")
#         full_fetch = cur.fetchall()
#         print(full_fetch)
# finally:
#     connect.close()

import sqlalchemy

print(sqlalchemy.__version__)

engine = sqlalchemy.create_engine(
    f'postgresql+psycopg2://postgres:{password}@{host}:5432/{dbname}'
)
connection = engine.connect()
result = connection.execute("SELECT * FROM employees")
for row in result:
    print('first_name', row['first_name'])

