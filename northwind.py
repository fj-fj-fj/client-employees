import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from misc import dbname, host, password


with open('northwind.sql', encoding='utf-8') as sql:
    northwind = sql.readlines()

def empty_line_or_comment(row):
    return '--' in row or not row.strip()

def start_multiline(row):
    return '(' in row[-2] or 'ALTER' in row

def end_multiline(row):
    return ');\n' == row or 'ADD' in row

def is_oneline_sql(row):
    return (';' == row[-2] and len(row) > 3)

def sql_parse(start=0):
    """Parse one or multi-line SQL commands"""
    sql_command = str()
    sql_commands = list()
    is_multiline: int = start
    for i, row in enumerate(northwind[start:]):
        if is_multiline:
            # склеить многострочную комманду
            sql_command += row.rstrip()
            if end_multiline(row):
                return sql_command
            continue
        if empty_line_or_comment(row):
            # пустые строки или комментарии
            continue
        if start_multiline(row):
            # многострочная SQL комманда
            row = row.rstrip()
            sql_command = sql_parse(i)
            sql_commands.append(sql_command)
        if is_oneline_sql(row):
            # однострочная SQL комманда
            sql_command = row.rstrip()
            if 'ADD' in row:
                # ограничения уже обработаны
                continue
            sql_commands.append(sql_command)
    return sql_commands

conn = psycopg2.connect(
    dbname=dbname, 
    user='postgres', 
    host=host, 
    password=password
)
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur = conn.cursor()
cur.execute('DROP DATABASE IF EXISTS northwind;')
cur.execute("CREATE DATABASE northwind;")
conn.commit()
cur.close()
# в след раз можно сразу так:
# CREATE USER test_user WITH password 'qwerty';
# CREATE DATABASE test_database OWNER test_user; или
# GRANT ALL privileges ON DATABASE test_db TO test_user;

try:
    with conn:
        with conn.cursor as cur:
            for command in sql_parse():
                cur.execute(command)
finally:
    conn.close()

try:
    with conn:
        with conn.cursor as cur:
            cur.execute("SELECT first_name FROM employees")
            all_names = cur.fetchall()
            assert 9 == len(all_names)
finally:
    conn.close()
