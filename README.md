#### Посмотреть библиотеки для работы с email, db, excel, soap ####


*  Создать soap сервис (*spyne*)
*  Получить данные по wsdl (*zeep*)
*  Сохранить данные в БД (*psycopg2-binary*, *SQLAlchemy*)
*  Получить имэйлы сотрудников из excel (*xlrd*, *pandas*)
*  Добавить сотрудников в excel (*xlwt*, *XlsxWriter*, *pandas*)
*  Отправить сотрудникам информацию на почту (*smptlib*)

(^_^♪)