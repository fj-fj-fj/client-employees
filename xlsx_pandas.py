# https://pandas.pydata.org/docs/
from datetime import datetime

import pandas as pd


data = [
    ('Liam', 'liam@gmail.com'), 
    ('Noah', 'noag@gmail.com'), 
    ('William', 'vedrupustoe@gmail.com'), 
    ('Olivia', 'olivia@gmail.com')
]
# для разделенных данных: [*zip(name, email)]
# для словарей [dict([e]) for e in data]

df = pd.DataFrame(data)
# df.to_excel('pandas.xlsx', header=False, index=False)

writer = pd.ExcelWriter("employees_pandas.xlsx", engine='xlsxwriter')

workbook  = writer.book
worksheet = workbook.add_worksheet('Employee1')

header_format = workbook.add_format({'bold': True})
date_format = workbook.add_format({'num_format': 'd mmmm yyyy'})

for i, row in enumerate(df.values):
    for j, val in enumerate(row):
        if not j: worksheet.write(i, j, val, header_format)
        else: worksheet.write(i, j, val)

worksheet.write_datetime(f'A{len(df.values)+2}', datetime.today(), date_format)
writer.save()


e = pd.read_excel('employees.xlsx', header=None, engine='openpyxl')
p = pd.read_excel('employees_pandas.xlsx', header=None, engine='openpyxl')

print(e.iloc[2] == p.iloc[2])
 