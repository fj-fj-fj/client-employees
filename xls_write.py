# https://xlwt.readthedocs.io/en/latest/index.html
from datetime import datetime

import xlwt


employees = [
    ('Liam', 'liam@gmail.com'), 
    ('Noah', 'noag@gmail.com'), 
    ('William', 'vedrupustoe@gmail.com'), 
    ('Olivia', 'olivia@gmail.com')
]

font0 = xlwt.Font()
font0.name = 'Times New Roman'
font0.bold = True

style0 = xlwt.XFStyle()
style0.font = font0

style1 = xlwt.XFStyle()
style1.num_format_str = 'D-MMM-YY'

wb = xlwt.Workbook()
ws = wb.add_sheet('Employee_Sheet')

for row, employee in enumerate(employees):
    name, mail = employee
    ws.write(row, 0, name, style0)
    ws.write(row, 1, mail)

ws.write(len(employees)+1, 0, datetime.now(), style1)

wb.save('employees.xls')
