from collections.abc import ValuesView
from typing import Tuple

import pandas as pd


def get_emails_from_excel() -> Tuple[ValuesView]:
    df_employees = pd.read_excel('employees.xlsx', header=None, engine='openpyxl')
    recipient = {'to': [], 'cc': [], 'bcc': []}
    try:
        for _, email, type_ in df_employees.values:
            recipient[type_].append(email)
    except KeyError:
        pass
    return recipient.values()
    # return recipient['to'], recipient['cc'], recipient['bcc']

if __name__ == '__main__':
    print(get_emails_from_excel)