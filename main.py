import logging
from typing import Tuple
from collections.abc import ValuesView

from simple_client import zeep_client
from simple_smtp import send_email
from xlsx_read import get_emails_from_excel


logging.basicConfig(level=logging.DEBUG)
logging.getLogger().setLevel(level=logging.DEBUG)

def _get_and_save_client_data() -> str:
    client_data = zeep_client()
    # ... use orm sqlalchemy ...
    return client_data

def _send_emails_to_employees(client_data: str, emails: Tuple[ValuesView]):
    subject = input('Enter subject: ') or 'client_data'
    to, cc, bcc = emails
    body: str = client_data
    send_email(subject, to, cc, bcc, body)

def main():
    """Get and save client data to the DB 
    and send info-emails to the employees
    """
    logging.debug('The function main()')

    client_data: str = _get_and_save_client_data()
    emails: Tuple[ValuesView] = get_emails_from_excel()
    _send_emails_to_employees(client_data, emails)

if __name__ == '__main__':
    main()

    logging.debug('Emails sended')
    print('loggers: ', logging.Logger.manager.loggerDict.__len__())
    # for key in logging.Logger.manager.loggerDict: print(key)
    