# https://xlrd.readthedocs.io/en/latest/index.html
import xlrd


book = xlrd.open_workbook("employees.xls")

print("The number of worksheets is {0}".format(book.nsheets))
print("Worksheet name(s): {0}".format(book.sheet_names()))

sh = book.sheet_by_index(0)

print("{0} {1} {2}".format(sh.name, sh.nrows, sh.ncols))
print("Cell A3 is {0}".format(sh.cell_value(rowx=2, colx=0)))
print("Cell B3 is {0}".format(sh.cell_value(rowx=2, colx=1)))

employees = []

for rx in range(sh.nrows):
    name, mail = sh.row(rx)
    if not name.value:
        break
    employees.append((
        name.value,
        mail.value
    ))
print(employees)

resepient_name = sh.cell_value(rowx=2, colx=0).capitalize()
 