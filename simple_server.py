# from lxml import etree
from spyne import Application, rpc, ServiceBase, String
from spyne.protocol.soap import Soap11
from spyne.protocol.json import JsonDocument
from spyne.server.wsgi import WsgiApplication


class Soap(ServiceBase):
    @rpc(String, _returns=String)
    def hello_name(ctx, name: str):
        # print(etree.tostring(ctx.in_document))
        return 'Hello, %s' % name

app = Application(
    [Soap], 'work.simple_server', in_protocol=Soap11(validator='lxml'), out_protocol=Soap11()
)
application = WsgiApplication(app)

if __name__ == '__main__':
    import logging
    from wsgiref.simple_server import make_server

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

    logging.info("listening to http://127.0.0.1:8000")
    logging.info("wsdl is at: http://localhost:8000/?wsdl")

    server = make_server('127.0.0.1', 8000, application)
    server.serve_forever()
    