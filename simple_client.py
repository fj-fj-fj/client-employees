import zeep

from xls_read import resepient_name


wsdl = 'http://localhost:8000/?wsdl'
client = zeep.Client(wsdl=wsdl)

for service in client.wsdl.services.values():
    print('Сервис:', service.name)
    for port in service.ports.values():
        for operation in port.binding._operations.values():
            print('Метод:', operation.name)
            print('    параметр:', operation.input.signature())

def zeep_client() -> str:
    return client.service.hello_name(resepient_name)

if __name__ == '__main__':
    from misc import emails
    from simple_smtp import send_email
    
    subject = 'HELLO VDIM'
    to = emails['to']
    cc = emails['cc']
    bcc = emails['bcc']
    body: str = zeep_client()
    send_email(subject, to, cc, bcc, body)
 