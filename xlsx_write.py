# https://xlsxwriter.readthedocs.io/
from datetime import datetime

import xlsxwriter

from misc import employees


workbook = xlsxwriter.Workbook('employees.xlsx')
worksheet = workbook.add_worksheet()

row = col = int()

bold = workbook.add_format({'bold': True})
date_format = workbook.add_format({'num_format': 'd mmmm yyyy'})

for name, email, type_ in employees:
    worksheet.write(row, col, name, bold)
    worksheet.write(row, col + 1, email)
    worksheet.write(row, col + 2, type_)
    row += 1

worksheet.write_datetime(f'A{len(employees)+2}', datetime.today(), date_format)

workbook.close()
 